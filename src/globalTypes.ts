type RobotFaceDirectionType = 'EAST' | 'NORTH' | 'SOUTH' | 'WEST' | undefined

export interface TableStateType {
  gridSize: number
  faceDirection?: RobotFaceDirectionType
  reportPosition?: boolean
  robotIsPlaced: boolean
  xcoord?: number
  ycoord?: number
}
